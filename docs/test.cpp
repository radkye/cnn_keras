#include <iostream>
#include <vector>

int main()
{
	std::vector<float> items{65, 66, 67};

	for (auto&& item : items)
	{
		std::cout << (char)item << std::endl;
	}

	std::vector<char> labels{'A', 'B', 'C'};

	for (auto&& label : labels)
	{
		std::cout << (float)label << std::endl;
	}

	return 0;
}