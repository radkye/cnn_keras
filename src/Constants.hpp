/** Copyright Radoslaw Zwolski 2017.
    Sign Language Recognition application for University purposes **/

#include <vector>

#ifndef ASL_CONSTANTS_
#define ASL_CONSTANTS_

namespace constants
{

const int THRESHOLD_LIMIT = 70;

const int SOURCE_WINDOW_WIDTH = 900;
const int SOURCE_WINDOW_HEIGHT = 900;
const int PROCESSED_WINDOW_WIDTH = 300;
const int PROCESSED_WINDOW_HEIGHT = 300;
const int RECTANGLE_HEIGHT = 400;
const int RECTANGLE_WIDTH = 400;

const int TRAINING_IMAGE_WIDTH = 60;
const int TRAINING_IMAGE_HEIGHT = 60;

const auto SKIN_MIN_COLOR = std::vector<int>({0, 133, 77});
const auto SKIN_MAX_COLOR = std::vector<int>({255, 173, 127});

}  // namespace constants

#endif  // ASL_CONSTANTS_
