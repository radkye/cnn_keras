/** Copyright Radoslaw Zwolski 2017.
    Sign Language Recognition application for University purposes.
    Master Project **/

#include <chrono>
#include <iostream>
#include <random>
#include <string>
#include <vector>
#include <fstream>

#include "opencv2/opencv.hpp"

#include "Constants.hpp"

const auto letterName = std::string("Y");
const int letterImagesAmount = 50;
const std::vector<char> letters{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
    'R', 'S', 'T', 'W', 'U', 'V', 'X', 'Y'};
const std::vector<char> trainingLetters{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M'};
const std::map<char, int> lettersMap {
    {'A', 0},
    {'B', 1},
    {'C', 2},
    {'D', 3},
    {'E', 4},
    {'F', 5},
    {'G', 6},
    {'H', 7},
    {'I', 8},
    {'K', 9},
    {'L', 10},
    {'M', 11}
};

const std::map<int, std::string> indicesMap {
    {0, "A"},
    {1, "B"},
    {2, "C"},
    {3, "D"},
    {4, "E"},
    {5, "F"},
    {6, "G"},
    {7, "H"},
    {8, "I"},
    {9, "K"},
    {10, "L"},
    {11, "M"}
};

int getRandomNumber(int lowerLimit, int higherLimit)
{
    std::random_device randomDevice;
    std::default_random_engine generator(randomDevice());
    std::uniform_int_distribution<int> distribution(lowerLimit, higherLimit);
    return distribution(generator);
}

cv::Mat processGrayImage(const cv::Mat& image)
{
    cv::Mat result, gaussian, xAxisGradient, yAxisGradient, absXAxisGradient, absYAxisGradient;

    cv::GaussianBlur(image, gaussian, cv::Size(3, 3), 0, 0, cv::BORDER_DEFAULT);
    cv::Sobel(gaussian, xAxisGradient, CV_16S, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
    cv::Sobel(gaussian, yAxisGradient, CV_16S, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
    convertScaleAbs(xAxisGradient, absXAxisGradient);
    convertScaleAbs(yAxisGradient, absYAxisGradient);
    cv::addWeighted(absXAxisGradient, 0.5, absYAxisGradient, 0.5, 0, result);

    return result;
}

int getHandContourIndex(const std::vector<std::vector<cv::Point>>& contours, int minimalArea)
{
    int biggestArea = minimalArea;
    unsigned biggestContourIndex = 0;
    unsigned index = 0;
    for (auto&& contour : contours)
    {
        auto area = cv::contourArea(contour);
        if (area > biggestArea)
        {
            biggestArea = area;
            biggestContourIndex = index;
        }
        index++;
    }
    return biggestContourIndex;
}

cv::Mat shuffleRows(const cv::Mat& trainingMatrix, const cv::Mat& trainingLabels,
    cv::Mat& shuffledTrainingLabels)
{
    std::vector <int> indices;
    for (int i = 0; i < trainingMatrix.rows; i++)
    {
        indices.push_back(i);
    }

    cv::randShuffle(indices);

    cv::Mat output;
    for (int i = 0; i < trainingMatrix.rows; i++)
    {
        output.push_back(trainingMatrix.row(indices[i]));
        shuffledTrainingLabels.push_back(trainingLabels.row(indices[i]));
    }

    return output;
}

void addImageToTrainedData(cv::Mat& trainingMatrix, const cv::Mat& readImage, int fileNumber)
{
    cv::Mat resized;
    cv::Mat handMask;
    cv::Mat handFieldImageYCrCb;
    cv::Mat handFieldImageYCrCbBlurred;
    cv::Mat grayScale;
    cv::Mat binaryImage;
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::Mat trainingImage;

    cv::cvtColor(readImage, handFieldImageYCrCb, cv::COLOR_BGR2YCrCb);
    cv::GaussianBlur(handFieldImageYCrCb, handFieldImageYCrCbBlurred, cv::Size(13, 13), 0, 0,
            cv::BORDER_DEFAULT);
    cv::inRange(handFieldImageYCrCbBlurred, constants::SKIN_MIN_COLOR, constants::SKIN_MAX_COLOR, handMask);
    cv::findContours(handMask, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

    if (!contours.empty())
    {
        auto handContourIndex = getHandContourIndex(contours, 3000);
        auto handContourMat = cv::Mat(contours[handContourIndex]);

        auto handRectangle = cv::boundingRect(handContourMat);
        trainingImage = readImage(handRectangle);
    }
    cv::cvtColor(trainingImage, grayScale, cv::COLOR_BGR2GRAY);
    auto sobelImage = processGrayImage(grayScale);
    cv::threshold(sobelImage, binaryImage, constants::THRESHOLD_LIMIT, 255, cv::THRESH_BINARY);
    cv::resize(binaryImage, resized, cv::Size(constants::TRAINING_IMAGE_WIDTH,
        constants::TRAINING_IMAGE_HEIGHT));
    
    int pixelIndex = 0;
    for (int i = 0; i < resized.rows; i++)
    {
        for (int j = 0; j < resized.cols; j++)
        {
            trainingMatrix.at<float>(fileNumber, pixelIndex++) = resized.at<uchar>(i, j);
        }
    }
}

void trainModel(cv::Ptr<cv::ml::ANN_MLP>& model)
{
    auto fileAmount = trainingLetters.size() * letterImagesAmount;
    cv::Mat trainingLabels = cv::Mat(fileAmount, trainingLetters.size(), CV_32F);
    trainingLabels = cv::Scalar(-1);
    std::cout << trainingLabels << std::endl;
    // std::vector<int> labels;
    // int labels[fileAmount];
    // cv::Mat labels(fileAmount, 1, CV_32SC1);

    auto trainingImageSize = constants::TRAINING_IMAGE_HEIGHT * constants::TRAINING_IMAGE_WIDTH;
    cv::Mat trainingMatrix(fileAmount, trainingImageSize, CV_32F);
    int fileNumber = 0;
    for (auto&& letter : trainingLetters)
    {
        for (int letterIndex = 0; letterIndex < letterImagesAmount; letterIndex++)
        {
            auto filePath = std::string("/home/radkye/Documents/studia/ASLRecognizer/images/") + letter + std::string("/") + letter + std::string("_") + std::to_string(letterIndex) + std::string(".jpg");
            std::cout << "File path: " << filePath << std::endl;
            cv::Mat readImage = cv::imread(filePath);
            addImageToTrainedData(trainingMatrix, readImage, fileNumber);
            trainingLabels.at<float>(fileNumber, lettersMap.at(letter)) = 1;
            // labels.push_back(static_cast<int>(letter));
            // labels.at<float>(fileNumber) = static_cast<float>(letter);
            fileNumber++;
        }
    }

    // auto labelsMat = cv::Mat(labels);

    std::cout << "TrainingLabels size: " << trainingLabels.size() << std::endl;
    // cv::Mat shuffledTrainingLabels;
    // auto shuffledTrainingMatrix = shuffleRows(trainingMatrix, trainingLabels, shuffledTrainingLabels);
    auto trainingData = cv::ml::TrainData::create(trainingMatrix, cv::ml::ROW_SAMPLE, trainingLabels);
    // auto trainingData = cv::ml::TrainData::create(shuffledTrainingMatrix, cv::ml::ROW_SAMPLE, shuffledTrainingLabels);
    // auto trainingData = cv::ml::TrainData::create(trainingMatrix, cv::ml::ROW_SAMPLE, labelsMat);
    std::cout << "Training of neural network ongoing. Please wait." << std::endl;
    
    auto start = std::chrono::system_clock::now();
    // auto end = std::chrono::system_clock::now();
    // auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start);
    // std::cout << elapsed.count() << std::endl;

    model->train(trainingData);

    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start);
    std::cout << "Time of learning[in seconds]: " << elapsed.count() << std::endl;
}

float getRecognizedGesture(cv::Ptr<cv::ml::StatModel> model, const cv::Mat& handFieldImage, cv::Mat& response,
    const cv::Rect& rectangle)
{
    // std::cout << "Before cutting rectangle" << std::endl;
    cv::Mat handImage = handFieldImage(rectangle);
    // std::cout << "After cutting rectangle" << std::endl;
    cv::Mat grayScale;
    cv::Mat binaryImage;
    cv::Mat resized;
    auto trainingImageSize = constants::TRAINING_IMAGE_HEIGHT * constants::TRAINING_IMAGE_WIDTH;
    cv::Mat predictionMatrix(1, trainingImageSize, CV_32F);
    cv::cvtColor(handImage, grayScale, cv::COLOR_BGR2GRAY);
    auto sobelImage = processGrayImage(grayScale);
    cv::threshold(sobelImage, binaryImage, constants::THRESHOLD_LIMIT, 255, cv::THRESH_BINARY);
    cv::resize(sobelImage, resized, cv::Size(constants::TRAINING_IMAGE_WIDTH,
        constants::TRAINING_IMAGE_HEIGHT));

    // std::cout << "Before preparation of matrix." << std::endl;

    int pixelIndex = 0;
    for (int i = 0; i < resized.rows; i++)
    {
        for (int j = 0; j < resized.cols; j++)
        {
            predictionMatrix.at<float>(0, pixelIndex++) = resized.at<uchar>(i, j);
        }
    }

    // std::cout << "SVM::getVarCount: " << model->getVarCount() << std::endl;

    // std::cout << "Before predict." << std::endl;

    // std::cout << "Prediction matrix size: [" << 1 << ", " << trainingImageSize << "]" << std::endl;
    model->predict(predictionMatrix, response);
    // std::cout << "After predict." << std::endl;
    // return model->predict(predictionMatrix);
    // std::cout << "After predict" << std::endl;
}

std::string saveImage(const cv::Mat& image)
{
    static unsigned letterIndex = 0;
    const auto filePath = std::string("/home/radkye/Documents/studia/ASLRecognizer/images/")
        + letterName + std::string("/") + letterName + std::string("_")
        + std::to_string(letterIndex) + std::string(".jpg");
    if (cv::imwrite(filePath, image))
    {
        letterIndex++;
        return std::string(filePath);
    }
    return std::string();
}

std::string getLetter(const cv::Mat& responses)
{
    std::vector<double> responsesVec(responses.cols);
    for (int i = 0; i < responses.cols; i++)
    {
        responsesVec[i] = responses.at<float>(0, i);
    }

    int greatestValueIndex = -1;
    for (int i = 0; i < responsesVec.size(); i++)
    {
        if (responsesVec[i] > 0.9 && responsesVec[i] > greatestValueIndex)
        {
            greatestValueIndex = i;
        }
    }
    
    return greatestValueIndex == -1 ? std::string("") : indicesMap.at(greatestValueIndex);
}

int main(int argc, char** argv)
{
    if (argc < 3)
    {
        return 1;
    }
    auto hiddenLayerSizeStr = std::string(argv[1]);
    std::cout << "Before MLP create." << std::endl;
    auto model = cv::ml::ANN_MLP::create();
    // cv::Mat layersSize = cv::Mat(3, 1, CV_32S);
    auto hiddenLayerSize = std::stod(hiddenLayerSizeStr);
    auto inputLayerSize = constants::TRAINING_IMAGE_HEIGHT * constants::TRAINING_IMAGE_WIDTH;
    auto outputLayerSize = trainingLetters.size();
    std::cout << inputLayerSize << " " << hiddenLayerSize << " " << outputLayerSize << std::endl;
    // layersSize.row(0) = inputLayerSize;
    // layersSize.row(1) = hiddenLayerSize;
    // layersSize.row(2) = trainingLetters.size();
    std::vector<int> layersSize {inputLayerSize, hiddenLayerSize, 100, outputLayerSize};
    auto terminationCriteria = cv::TermCriteria(
        cv::TermCriteria::Type::EPS,
        10000,
        0.0001);
    std::cout << "Before setLayerSize." << std::endl;
    model->setLayerSizes(layersSize);
    model->setActivationFunction(cv::ml::ANN_MLP::ActivationFunctions::SIGMOID_SYM, 1.0, 1.0);
    model->setTrainMethod(cv::ml::ANN_MLP::TrainingMethods::BACKPROP, 0.05, 0.05);
    model->setTermCriteria(terminationCriteria);
    // auto readSize = model->getLayerSizes();
    // std::cout << "Total: " << readSize.total() << std::endl;
    // for (auto&& layer : readSize)
    // {
    //     std::cout << layer << std::endl;
    // }
    // std::cout << "Mat empty(): " << readSize.empty();


    // ============================================== SVM START =============================================

    // auto model = cv::ml::SVM::create();
    // auto terminationCriteria = cv::TermCriteria(
    //     cv::TermCriteria::Type::COUNT + cv::TermCriteria::Type::EPS,
    //     10000,
    //     0.0001);
    // model->setTermCriteria(terminationCriteria);
    // model->setType(cv::ml::SVM::C_SVC);
    // model->setKernel(cv::ml::SVM::RBF);
    // model->setC(10000000);
    // model->setDegree(3);
    // model->setGamma(3);
    trainModel(model);
    auto modelFilePath = std::string(argv[2]);
    if (model->isTrained())
    {
        model->save(modelFilePath);
        std::cout << "Neural Network has been trained." << std::endl;
    }


    cv::VideoCapture videoCapture(0); // open the default camera
    if(!videoCapture.isOpened())  // check if we succeeded
    {
        std::cout << "Failed to open image from camera." << std::endl;
        return -1;
    }

    cv::Mat edges;
    cv::Mat sourceGrayScale;
    cv::Mat sourceBlurred;
    cv::Mat sourceEdged;
    cv::Mat xAxisGradient;
    cv::Mat yAxisGradient;
    cv::Mat absXAxisGradient;
    cv::Mat absYAxisGradient;
    cv::Mat fullGradient;

    cv::Rect rectangle(cv::Point(0, 0), cv::Point(constants::RECTANGLE_WIDTH,
        constants::RECTANGLE_HEIGHT));

    cv::namedWindow("Sobel Edge Detector", cv::WINDOW_NORMAL);
    cv::resizeWindow("Sobel Edge Detector", constants::PROCESSED_WINDOW_WIDTH, constants::PROCESSED_WINDOW_HEIGHT);
    cv::namedWindow("Camera Original Capture", cv::WINDOW_NORMAL);
    cv::resizeWindow("Camera Original Capture", constants::SOURCE_WINDOW_WIDTH, constants::SOURCE_WINDOW_HEIGHT);
    cv::namedWindow("Hand Field", cv::WINDOW_NORMAL);
    cv::resizeWindow("Hand Field", constants::RECTANGLE_WIDTH, constants::RECTANGLE_HEIGHT);
    cv::namedWindow("Hand Image", cv::WINDOW_NORMAL);
    cv::resizeWindow("Hand Image", constants::RECTANGLE_WIDTH, constants::RECTANGLE_HEIGHT);

    for(;;)
    {
        cv::Mat sourceFrame;
        videoCapture >> sourceFrame; // get a new frame from camera
        cv::cvtColor(sourceFrame, sourceGrayScale, cv::COLOR_BGR2GRAY); // convert to gray scale

        cv::Mat handFieldImage = sourceFrame(rectangle);
        cv::Mat handFieldImageYCrCb;
        cv::Mat handFieldImageYCrCbBlurred;
        cv::Mat handMask;
        cv::Mat handImage = cv::Mat::zeros(handFieldImage.size(), CV_8UC3);
        std::vector<std::vector<cv::Point>> contours;
        std::vector<cv::Vec4i> hierarchy;

        cv::Rect handRectangle;

        cv::cvtColor(handFieldImage ,handFieldImageYCrCb, cv::COLOR_BGR2YCrCb);
        cv::GaussianBlur(handFieldImageYCrCb, handFieldImageYCrCbBlurred, cv::Size(13, 13), 0, 0,
            cv::BORDER_DEFAULT);
        cv::inRange(handFieldImageYCrCbBlurred, constants::SKIN_MIN_COLOR, constants::SKIN_MAX_COLOR, handMask);
        cv::Mat crop(handFieldImage.rows, handFieldImage.cols, CV_8UC3);
        cv::findContours(handMask, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);
        if (!contours.empty())
        {
            auto handContourIndex = getHandContourIndex(contours, 3000);
            auto handContourMat = cv::Mat(contours[handContourIndex]);

            std::vector<std::vector<cv::Point>> myContour {contours[handContourIndex]};
            cv::Mat mask = cv::Mat::zeros(handFieldImage.rows, handFieldImage.cols, CV_8UC1);
            drawContours(mask, myContour, -1, cv::Scalar(255), cv::FILLED);

            // set background to green
            crop.setTo(cv::Scalar(0));
            handFieldImage.copyTo(crop, mask);
            cv::normalize(mask.clone(), mask, 0.0, 255.0, cv::NORM_MINMAX, CV_8UC1);

            handRectangle = cv::boundingRect(handContourMat);

            std::vector<cv::Point> hull;
            // cv::convexHull(handContourMat, hull, false);

            auto hullMat = cv::Mat(hull);
            cv::drawContours(sourceFrame, {hullMat}, -1, {255, 0, 0}, 2);

            handFieldImage.copyTo(handImage, handMask);
            cv::drawContours(sourceFrame, {handContourMat}, -1, {0, 0, 255});

            cv::rectangle(sourceFrame, rectangle, {255, 0, 0}, 2); // draw rectangle
            cv::rectangle(sourceFrame, handRectangle, {0, 255, 0});

            cv::GaussianBlur(sourceGrayScale, sourceBlurred, cv::Size(3, 3), 0, 0, cv::BORDER_DEFAULT);

            cv::Sobel(sourceBlurred, xAxisGradient, CV_16S, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
            cv::Sobel(sourceBlurred, yAxisGradient, CV_16S, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
            convertScaleAbs(xAxisGradient, absXAxisGradient);
            convertScaleAbs(yAxisGradient, absYAxisGradient);

            cv::addWeighted(absXAxisGradient, 0.5, absYAxisGradient, 0.5, 0, fullGradient);

            // cv::imshow("Sobel Edge Detector", fullGradient);

            cv::Mat response(1, trainingLetters.size(), CV_32F);

            getRecognizedGesture(model, handFieldImage, response, handRectangle);

            auto recognizedLetter = getLetter(response);
            std::cout << "Recognized letter: " << recognizedLetter << std::endl;
            putText(sourceFrame, recognizedLetter, cv::Point(400, 400),
                cv::FONT_HERSHEY_COMPLEX, 2, cv::Scalar(0, 255, 0), 1, cv::LINE_AA);
        }
        cv::imshow("Hand Image", crop);
        cv::imshow("Hand Field", handFieldImage);
        cv::imshow("Camera Original Capture", sourceFrame); // show camera image

        if (cv::waitKey(1) == 120)
        {
            std::cout << "Exit button has been pressed. Exiting application." << std::endl;
            break;
        }

        if (cv::waitKey(25) == 115)
        {
            std::cout << "+++++++++++++++++++++           Saving File             +++++++++++++++++++++++";
            auto savedImagePath = saveImage(crop);
            if (!savedImagePath.empty())
            {
                std::cout << "Could not save file: " << savedImagePath << std::endl;
            }
        }
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}
