import cv2
import numpy as np
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from keras.utils import to_categorical
import operator
import time

image_size = 60
camera_id = 0
video_capture = cv2.VideoCapture(camera_id)

training_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'W', 'U', 'V', 'X', 'Y']
letters_to_indices_map = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7,
    'I': 8, 'K': 9, 'L': 10, 'M': 11, 'N': 12, 'O': 13, 'P': 14, 'Q': 15, 'R': 16,
    'S': 17, 'T': 18, 'W': 19, 'U': 20, 'V': 21, 'X': 22, 'Y': 23}

def get_biggest_contour(contours, minimal_contour_size = 4000):
    size = 0
    result = []
    temp_id = -1
    contour_id = 0
    for contour in contours:
        temp_id = temp_id + 1
        temp_size = cv2.contourArea(contour)
        if temp_size > size:
            size = temp_size
            result = contour
            contour_id = temp_id

    return result, contour_id

def get_recognized_letter(letter_index):
    for letter in training_letters:
        if letters_to_indices_map[letter] == letter_index:
            return letter
    return " "

def get_result_index(results):
    index = -1
    temp_result = -1
    for result in results:
        if result > temp_result:
            temp_result = result
            index = index + 1
    return index

def load_training_and_test_data(training_letter_max_id, training_image_size):
    training_set = []
    labels = []
    for letter in training_letters:
        for i in range(0, training_letter_max_id):
            letter_file_path = '/home/radkye/Documents/studia/ASLRecognizer/images/' + letter + '/' + letter + '_' + str(i) + '.jpg'
            print('Loading data: ' + letter_file_path)
            read_image = cv2.imread(letter_file_path, -1)

            blur = cv2.GaussianBlur(read_image, (7, 7), 0)
            ycrcb_img = cv2.cvtColor(blur, cv2.COLOR_BGR2YCR_CB)
            
            ycrcb_skin_min = np.array([0, 133, 77])
            ycrcb_skin_max = np.array([255, 173, 127])
            
            mask = cv2.inRange(ycrcb_img, ycrcb_skin_min, ycrcb_skin_max)
            hand_image = cv2.bitwise_and(read_image, read_image, mask = mask)
            
            contour_image_whatever, contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, 2)
    
            if contours != None:
                biggest_contour, biggest_contour_id = get_biggest_contour(contours, 4000)
                # cv2.drawContours(input_img, contours, biggest_contour_id, (0, 0, 255), 1)
                if biggest_contour != []:
                    hand_contour_bounding_rect = cv2.boundingRect(biggest_contour)
                    x, y, w, h = hand_contour_bounding_rect
                    if w > 0 and h > 0:
                        # cv2.rectangle(input_img, (x, y), (x + w, y + h), (0, 255, 0), 1)
                        rectangle_hand_image = hand_image[y : y+h, x : x+w]

            grayscale_hand_img = cv2.cvtColor(rectangle_hand_image, cv2.COLOR_BGR2GRAY)
            gx = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 1, 0)
            gy = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 0, 1)
            sobel_sum = cv2.addWeighted(gx, 0.5, gy, 0.5, 0)
            ret, thresholded_sobel = cv2.threshold(sobel_sum, 70, 255, cv2.THRESH_BINARY)
            resized = cv2.resize(thresholded_sobel, (training_image_size, training_image_size))
            resized_row = []
            for x in resized:
                for y in x:
                    resized_row.append(y)
            training_set.append(resized_row)

            label = letters_to_indices_map[letter]
            labels.append(label)
    
    print('Finished loading data.')
    return training_set, labels

training_set, labels = load_training_and_test_data(50, image_size)

train_classes = np.unique(labels)
train_nClasses = len(train_classes)
labels = to_categorical(labels, num_classes=train_nClasses)
print(labels)

ann = cv2.ml.ANN_MLP_create()
input_layer_size = image_size * image_size
output_layer_size = len(training_letters)
ann.setLayerSizes(np.array([input_layer_size, 200, 100, output_layer_size], dtype=np.uint8))
ann.setTrainMethod(cv2.ml.ANN_MLP_BACKPROP)

training_data = np.array(training_set, dtype=np.float32)
# training_data = np.transpose(training_data)
training_labels = np.array(labels, dtype=np.float32)
# training_labels = np.transpose(training_labels)

for i in range(0, len(training_labels)):
    training_labels[i] = [x if x != 0 else -1 for x in training_labels[i]]

print(training_labels.shape)
print(training_data.shape)
print('MLP is learning')
start_time = time.time()
ann.train(training_data,
    cv2.ml.COL_SAMPLE,
    training_labels)

elapsed_time = time.time() - start_time
print('Time of learning was: ' + elapsed_time)
time.sleep(5)

while(video_capture.isOpened()):
    _, input_img = video_capture.read()
    
    hand_field_img = input_img[0:400, 0:400]
    blur = cv2.GaussianBlur(hand_field_img, (7, 7), 0)
    ycrcb_img = cv2.cvtColor(blur, cv2.COLOR_BGR2YCR_CB)
    
    ycrcb_skin_min = np.array([0, 133, 77])
    ycrcb_skin_max = np.array([255, 173, 127])
    
    mask = cv2.inRange(ycrcb_img, ycrcb_skin_min, ycrcb_skin_max)
    hand_image = cv2.bitwise_and(hand_field_img, hand_field_img, mask = mask)
    
    contour_image_whatever, contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, 2)
    
    if contours != None:
        biggest_contour, biggest_contour_id = get_biggest_contour(contours, 4000)
        cv2.drawContours(input_img, contours, biggest_contour_id, (0, 0, 255), 1)
        if biggest_contour != []:
            hand_contour_bounding_rect = cv2.boundingRect(biggest_contour)
            x, y, w, h = hand_contour_bounding_rect
            if w > 0 and h > 0:
                cv2.rectangle(input_img, (x, y), (x + w, y + h), (0, 255, 0), 1)
                rectangle_hand_image = hand_image[y : y+h, x : x+w]

    # 	cv2.imshow('PredictedGesture', gesture)				  # showing the best match or prediction
    # 	cv2.putText(img, label,(50,150), font,8,(0,125,155), 2)  # displaying the predicted letter on the main screen
    # 	cv2.putText(img,text,(50,450), font,3,(0,0,255),2)
    cv2.rectangle(input_img, (0, 0), (400, 400), (255, 0, 0), 2)
    cv2.imshow('Camera capture', input_img)
    # cv2.imshow('Hand Field', hand_field_img)
    cv2.imshow('Hand Mask', mask)
    # cv2.imshow('Hand Image', hand_image)

    grayscale_hand_img = cv2.cvtColor(rectangle_hand_image, cv2.COLOR_BGR2GRAY)
    gx = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 1, 0)
    gy = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 0, 1)
    sobel_sum = cv2.addWeighted(gx, 0.5, gy, 0.5, 0)
    ret, thresholded_sobel = cv2.threshold(sobel_sum, 70, 255, cv2.THRESH_BINARY)
    resized = cv2.resize(thresholded_sobel, (image_size, image_size))
    resized_row = []
    for x in resized:
        for y in x:
            resized_row.append(y)

    print(ann.predict(np.array([resized_row], dtype=np.float32)))

    # recognition_result = model.predict(image_for_recognition)[0]
    # index, value = max(enumerate(recognition_result), key=operator.itemgetter(1))

    # recognized_letter = get_recognized_letter(index)

    # print(recognition_result)
    # print('Recognized letter is: ', recognized_letter)

    cv2.imshow('Camera capture', grayscale_hand_img)
    
    k = 0xFF & cv2.waitKey(10)
    if k == 120:
        print("Exiting the application.")
        break
    if k == 115:
        print("Saving hand image")
        cv2.imwrite('/home/radkye/Documents/masterThesis/images/handFieldOriginal.jpg', hand_field_img)
        cv2.imwrite('/home/radkye/Documents/masterThesis/images/blurHand.jpg', blur)
        cv2.imwrite('/home/radkye/Documents/masterThesis/images/mask.jpg', mask)
        break

if (not video_capture.isOpened()):
    print("Camera was not detected.")

video_capture.release()        
cv2.destroyAllWindows()