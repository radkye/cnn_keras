import cv2
import numpy as np
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from decimal import Decimal, ROUND_HALF_UP
import operator

image_size = 60
camera_id = 0
video_capture = cv2.VideoCapture(camera_id)
RIGHT_INDEX = 200
LEFT_INDEX = 180

training_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'W', 'U', 'V', 'X', 'Y']
letters_to_indices_map = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7,
    'I': 8, 'K': 9, 'L': 10, 'M': 11, 'N': 12, 'O': 13, 'P': 14, 'Q': 15, 'R': 16,
    'S': 17, 'T': 18, 'W': 19, 'U': 20, 'V': 21, 'X': 22, 'Y': 23}

def print_table_to_file(file_path, statistic, statistic_percentage, recognition_ratio, goodRecognitionAmount):
    file_object = open(file_path, 'w')

    file_object.write("\\begin{landscape}\n")
    file_object.write("\\thispagestyle{empty}\n")
    file_object.write("\\begin{table}\n")
    file_object.write("\\caption{}\n")
    begin_table_1 = "\\begin{tabular}{|"
    for i in range(0, len(training_letters) + 2):
        begin_table_1 = begin_table_1 + " c |"
    
    begin_table_1 = begin_table_1 + "}\n"
    file_object.write(begin_table_1)
    
    file_object.write("\\hline\n")

    letters_columns_1 = ""
    for letter in training_letters:
        letters_columns_1 = letters_columns_1 + "& \\textbf{" + letter + "} "
    letters_columns_1 = letters_columns_1 + " & "

    file_object.write(letters_columns_1 + "\\\\ \\hline \n")

    # our_value = Decimal()
    # output = Decimal(our_value.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))

    for letter in training_letters:
        letter_row = ""
        file_object.write(letter)
        letter_map = statistic[letter]
        for recognized_letter in training_letters:
            recognition_amount = letter_map[recognized_letter]
            if letter == recognized_letter:
                letter_row = letter_row + " & \\textbf{" + str(recognition_amount) + "}"
            else:
                letter_row = letter_row + " & " + str(recognition_amount)

        letter_row = letter_row + " & " + str(letter_map[' '])
        
        letter_row = letter_row + "\\\\ \\hline \n"
        file_object.write(letter_row)

    file_object.write("\\end{tabular}\n")
    file_object.write("\\end{table}\n")
    file_object.write("\\end{landscape}\n")

    amountOfSamplesPerLetter = RIGHT_INDEX - LEFT_INDEX
    amountOfTestingSamples = amountOfSamplesPerLetter * len(training_letters);
    file_object.write("Result: " + str(goodRecognitionAmount) + "/" + str(amountOfTestingSamples) + "\n")
    file_object.write("Result percentage: " + str(float(goodRecognitionAmount)/float(amountOfTestingSamples)))

    file_object.write("\n\n\n\n")

    file_object.write("\\begin{landscape}\n")
    file_object.write("\\thispagestyle{empty}\n")
    file_object.write("\\begin{table}\n")
    file_object.write("\\caption{}\n")
    begin_table_1 = "\\begin{tabular}{|"
    for i in range(0, len(training_letters) + 2):
        begin_table_1 = begin_table_1 + " c |"
    
    begin_table_1 = begin_table_1 + "}\n"
    file_object.write(begin_table_1)
    
    file_object.write("\\hline\n")

    letters_columns_1 = ""
    for letter in training_letters:
        letters_columns_1 = letters_columns_1 + "& \\textbf{" + letter + "} "
    letters_columns_1 = letters_columns_1 + " & "
    
    file_object.write(letters_columns_1 + "\\\\ \\hline \n")

    percents_line = "&"
    for i in range(0, len(training_letters) + 2):
        begin_table_1 = begin_table_1 + " & [\%]"

    # our_value = Decimal()
    # output = Decimal(our_value.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))

    for letter in training_letters:
        letter_row = ""
        file_object.write(letter)
        letter_map = statistic_percentage[letter]
        for recognized_letter in training_letters:
            recognition_amount = letter_map[recognized_letter]
            if letter == recognized_letter:
                letter_row = letter_row + " & \\textbf{" + str(int(recognition_amount)) + "}"
            else:
                letter_row = letter_row + " & " + str(int(recognition_amount))
        
        letter_row = letter_row + " & " + str(int(letter_map[' ']))

        letter_row = letter_row + "\\\\ \\hline \n"
        file_object.write(letter_row)

    file_object.write("\\end{tabular}\n")
    file_object.write("\\end{table}\n")
    file_object.write("\\end{landscape}\n")


def get_biggest_contour(contours, minimal_contour_size = 4000):
    size = 0
    result = []
    temp_id = -1
    contour_id = 0
    for contour in contours:
        temp_id = temp_id + 1
        temp_size = cv2.contourArea(contour)
        if temp_size > size:
            size = temp_size
            result = contour
            contour_id = temp_id

    return result, contour_id

def get_recognized_letter(letter_index):
    for letter in training_letters:
        if letters_to_indices_map[letter] == letter_index:
            return letter
    return " "

def get_result_index(results):
    index = -1
    temp_result = -1
    for result in results:
        if result > temp_result and result > 0.9:
            temp_result = result
            index = index + 1
    return index

model = load_model('/home/radkye/Documents/studia/cnn_keras/cnn/cnn_canny_200_100_d500_d200_samples_180_kernels_6_cropped_hand.h5')

statistic_dict = dict()
for letter in training_letters:
    statistic_dict[letter] = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0,
    'I': 0, 'K': 0, 'L': 0, 'M': 0, 'N': 0, 'O': 0, 'P': 0, 'Q': 0, 'R': 0,
    'S': 0, 'T': 0, 'W': 0, 'U': 0, 'V': 0, 'X': 0, 'Y': 0, ' ': 0}

statistic_percentage_dict = dict()
for letter in training_letters:
    statistic_percentage_dict[letter] = {'A': 0.0, 'B': 0.0, 'C': 0.0, 'D': 0.0, 'E': 0.0, 'F': 0.0, 'G': 0.0, 'H': 0.0,
    'I': 0.0, 'K': 0.0, 'L': 0.0, 'M': 0.0, 'N': 0.0, 'O': 0.0, 'P': 0.0, 'Q': 0.0, 'R': 0.0,
    'S': 0.0, 'T': 0.0, 'W': 0.0, 'U': 0.0, 'V': 0.0, 'X': 0.0, 'Y': 0.0, ' ': 0.0}


good_recognition_amount = 0

for letter in training_letters:
    for i in range(LEFT_INDEX, RIGHT_INDEX):
        letter_file_path = '/home/radkye/Documents/studia/ASLRecognizer/images/' + letter + '/' + letter + '_' + str(i) + '.jpg'
        read_image = cv2.imread(letter_file_path, -1)
        blur = cv2.GaussianBlur(read_image, (7, 7), 0)
        ycrcb_img = cv2.cvtColor(blur, cv2.COLOR_BGR2YCR_CB)
        
        ycrcb_skin_min = np.array([0, 133, 77])
        ycrcb_skin_max = np.array([255, 173, 127])
        
        mask = cv2.inRange(ycrcb_img, ycrcb_skin_min, ycrcb_skin_max)
        hand_image = cv2.bitwise_and(read_image, read_image, mask = mask)
        
        contour_image_whatever, contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, 2)

        if contours != None:
            biggest_contour, biggest_contour_id = get_biggest_contour(contours, 4000)
            # cv2.drawContours(input_img, contours, biggest_contour_id, (0, 0, 255), 1)
            if biggest_contour != []:
                hand_contour_bounding_rect = cv2.boundingRect(biggest_contour)
                x, y, w, h = hand_contour_bounding_rect
                if w > 0 and h > 0:
                    # cv2.rectangle(input_img, (x, y), (x + w, y + h), (0, 255, 0), 1)
                    rectangle_hand_image = hand_image[y : y+h, x : x+w]

        grayscale_hand_img = cv2.cvtColor(rectangle_hand_image, cv2.COLOR_BGR2GRAY)
        gx = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 1, 0)
        gy = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 0, 1)
        sobel_sum = cv2.addWeighted(gx, 0.5, gy, 0.5, 0)
        # canny_img = cv2.Canny(grayscale_hand_img, 70, 250)
        ret, thresholded_sobel = cv2.threshold(sobel_sum, 70, 255, cv2.THRESH_BINARY)
        resized = cv2.resize(thresholded_sobel, (image_size, image_size))

        normalized_to_one = resized.astype("float") / 255.0
        img_array = img_to_array(normalized_to_one)
        image_for_recognition = np.expand_dims(img_array, axis=0)

        recognition_result = model.predict(image_for_recognition)[0]
        index, max_value = max(enumerate(recognition_result), key=operator.itemgetter(1))

        if max_value > 0.98:
            recognized_letter = get_recognized_letter(index)
        else:
            recognized_letter = " "
        # print(recognition_result)
        print(recognized_letter)
        if recognized_letter == letter:
            good_recognition_amount = good_recognition_amount + 1
        statistic_dict[letter][recognized_letter] = statistic_dict[letter][recognized_letter] + 1

for letter in training_letters:
    for recognized_letter in training_letters:
        statistic_percentage_dict[letter][recognized_letter] = (statistic_dict[letter][recognized_letter]/float(RIGHT_INDEX - LEFT_INDEX)) * 100
    statistic_percentage_dict[letter][' '] = (statistic_dict[letter][' ']/float(RIGHT_INDEX - LEFT_INDEX)) * 100

test_samples_amount = (RIGHT_INDEX - LEFT_INDEX) * len(training_letters)
print("Result: " + str(good_recognition_amount) + "/" + str(test_samples_amount))

print(str(float(good_recognition_amount)/float(test_samples_amount)) + "%")

print_table_to_file('/home/radkye/Documents/studia/cnn_keras/cnn/cnn_canny_200_100_d500_d200_samples_180_kernels_10_cropped_hand.txt',
    statistic_dict, statistic_percentage_dict, str(float(good_recognition_amount)/float(test_samples_amount)),
    good_recognition_amount)
    
cv2.destroyAllWindows()