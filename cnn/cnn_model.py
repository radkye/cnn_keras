from __future__ import print_function
import numpy as np
import cv2
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras.datasets import cifar10
from keras.utils import to_categorical
from keras import backend as K


training_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'W', 'U', 'V', 'X', 'Y']
letters_to_indices_map = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7,
    'I': 8, 'K': 9, 'L': 10, 'M': 11, 'N': 12, 'O': 13, 'P': 14, 'Q': 15, 'R': 16,
    'S': 17, 'T': 18, 'W': 19, 'U': 20, 'V': 21, 'X': 22, 'Y': 23}

img_rows = img_cols = 60

def load_training_and_test_data(training_letter_max_id, training_image_size,
    left_test_index, right_test_index):

    training_samples_amount = training_letter_max_id * len(training_letters)
    test_samples_amount = (right_test_index - left_test_index - 1) * len(training_letters)

    train_input = np.empty((training_samples_amount, training_image_size, training_image_size),
        dtype='uint8')
    train_output = np.empty((training_samples_amount), dtype='uint8')

    test_input = np.empty((test_samples_amount, training_image_size, training_image_size),
        dtype='uint8')
    test_output = np.empty((test_samples_amount), dtype='uint8')

    print('Shape: ', test_output.shape)

    train_file_index = 0
    for letter in training_letters:
        for i in range(0, training_letter_max_id):
            letter_file_path = '/home/radkye/Documents/studia/ASLRecognizer/images/' + letter + '/' + letter + '_' + str(i) + '.jpg'
            read_image = cv2.imread(letter_file_path, 0)
            resized = cv2.resize(read_image, (training_image_size, training_image_size))
            train_input[train_file_index, :, :] = resized
            train_output[train_file_index] = letters_to_indices_map[letter]
            train_file_index = train_file_index + 1
    
    test_file_index = 0
    for letter in training_letters:
        for i in range(left_test_index, right_test_index - 1):
            letter_file_path = '/home/radkye/Documents/studia/ASLRecognizer/images/' + letter + '/' + letter + '_' + str(i) + '.jpg'
            read_image = cv2.imread(letter_file_path, 0)
            resized = cv2.resize(read_image, (training_image_size, training_image_size))
            test_input[test_file_index, :, :] = resized
            test_output[test_file_index] = letters_to_indices_map[letter]
            test_file_index = test_file_index + 1

    print('Test samples: ', test_samples_amount)
    print('Training samples: ', training_samples_amount)

    train_input = train_input.astype('float32')
    test_input = test_input.astype('float32')
    train_input /= 255
    test_input /= 255
    return (train_input, train_output), (test_input, test_output)

# for x in test_labels:
#     print(x)

# for x in train_output[4200]:
#     print(x)

(train_images, train_labels), (test_images, test_labels) = load_training_and_test_data(180, img_rows, 181, 200)

if K.image_data_format() == 'channels_first':
    train_images = train_images.reshape(train_images.shape[0], 1, img_rows, img_cols)
    test_images = test_images.reshape(test_images.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
    print('Channels first.')
else:
    print('Channels last.')
    train_images = train_images.reshape(train_images.shape[0], img_rows, img_cols, 1)
    test_images = test_images.reshape(test_images.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

print('Training data shape : ', train_images.shape, train_labels.shape)

print('Testing data shape : ', test_images.shape, test_labels.shape)

# Find the unique numbers from the train labels
classes = np.unique(train_labels)
nClasses = len(classes)
print('Total number of outputs : ', nClasses)
print('Output classes : ', classes)

# plt.figure(figsize=[4,2])

# # Display the first image in training data
# plt.subplot(121)
# plt.imshow(train_images[0, :, :], cmap='gray')
# plt.title("Ground Truth : {}".format(train_labels[0]))

# # Display the first image in testing data
# plt.subplot(122)
# plt.imshow(test_images[0,:,:], cmap='gray')
# plt.title("Ground Truth : {}".format(test_labels[0]))

# Find the shape of input images and create the variable input_shape
nRows,nCols,nDims = train_images.shape[1:]
train_data = train_images.reshape(train_images.shape[0], nRows, nCols, nDims)
test_data = test_images.reshape(test_images.shape[0], nRows, nCols, nDims)
input_shape = (nRows, nCols, nDims)

# Change to float datatype
train_data = train_data.astype('float32')
test_data = test_data.astype('float32')

# Scale the data to lie between 0 to 1
train_data /= 255
test_data /= 255

# Change the labels from integer to categorical data
train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)

# Display the change for category label using one-hot encoding
print('Original label 0 : ', train_labels[0])
print('After conversion to categorical ( one-hot ) : ', train_labels_one_hot[0])

def createModel():
    model = Sequential()
    # The first two layers with 32 filters of window size 3x3
    model.add(Conv2D(64, (5, 5), padding='same', activation='relu', input_shape=input_shape))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nClasses, activation='softmax'))
    
    return model


model1 = createModel()
batch_size = 1024
epochs = 50
model1.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

model1.summary()

history = model1.fit(train_data, train_labels_one_hot, batch_size=batch_size, epochs=epochs, verbose=1, 
                   validation_data=(test_data, test_labels_one_hot))
model1.evaluate(test_data, test_labels_one_hot)

plt.figure(figsize=[8, 6])
plt.plot(history.history['loss'], 'r', linewidth=3.0)
plt.plot(history.history['val_loss'], 'b', linewidth=3.0)
plt.legend(['Training loss', 'Validation Loss'], fontsize=18)
plt.xlabel('Epochs ', fontsize=16)
plt.ylabel('Loss', fontsize=16)
plt.title('Loss Curves', fontsize=16)

plt.figure(figsize=[8, 6])
plt.plot(history.history['acc'], 'r', linewidth=3.0)
plt.plot(history.history['val_acc'], 'b', linewidth=3.0)
plt.legend(['Training Accuracy', 'Validation Accuracy'], fontsize=18)
plt.xlabel('Epochs ', fontsize=16)
plt.ylabel('Accuracy', fontsize=16)
plt.title('Accuracy Curves', fontsize=16)

model.save('/home/radkye/Documents/studia/ASLRecognizer_cnn_keras/cnn/first_model.h5')  # creates a HDF5 file 'my_model.h5'
del model  # deletes the existing model

# returns a compiled model
# identical to the previous one
model = load_model('my_model.h5')


