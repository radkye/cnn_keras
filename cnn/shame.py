statistic_dict = dict()
for letter in training_letters:
    statistic_dict[letter] = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0,
    'I': 0, 'K': 0, 'L': 0, 'M': 0, 'N': 0, 'O': 0, 'P': 0, 'Q': 0, 'R': 0,
    'S': 0, 'T': 0, 'W': 0, 'U': 0, 'V': 0, 'X': 0, 'Y': 0}

statistic_percentage_dict = dict()
for letter in training_letters:
    statistic_percentage_dict[letter] = {'A': 0.0, 'B': 0.0, 'C': 0.0, 'D': 0.0, 'E': 0.0, 'F': 0.0, 'G': 0.0, 'H': 0.0,
    'I': 0.0, 'K': 0.0, 'L': 0.0, 'M': 0.0, 'N': 0.0, 'O': 0.0, 'P': 0.0, 'Q': 0.0, 'R': 0.0,
    'S': 0.0, 'T': 0.0, 'W': 0.0, 'U': 0.0, 'V': 0.0, 'X': 0.0, 'Y': 0.0}

good_recognition_amount = 0

for letter in training_letters:
    for i in range(LEFT_INDEX, RIGHT_INDEX):
        letter_file_path = '/home/radkye/Documents/studia/ASLRecognizer/images/' + letter + '/' + letter + '_' + str(i) + '.jpg'
        read_image = cv2.imread(letter_file_path, 0)
        blur = cv2.GaussianBlur(read_image, (7, 7), 0)
        gx = cv2.Sobel(blur, cv2.CV_32F, 1, 0)
        gy = cv2.Sobel(blur, cv2.CV_32F, 0, 1)
        sobel_sum = cv2.addWeighted(gx, 0.5, gy, 0.5, 0)
        ret, thresholded_sobel = cv2.threshold(sobel_sum, 70, 255, cv2.THRESH_BINARY)
        resized = cv2.resize(thresholded_sobel, (image_size, image_size))
        normalized_to_one = resized.astype("float") / 255.0
        img_array = img_to_array(normalized_to_one)
        image_for_recognition = np.expand_dims(img_array, axis=0)

        recognition_result = model.predict(image_for_recognition)[0]
        index, value = max(enumerate(recognition_result), key=operator.itemgetter(1))
        # index = get_result_index(recognition_result)

        recognized_letter = get_recognized_letter(index)
        # print(recognition_result)
        print(recognized_letter)
        if recognized_letter == letter:
            good_recognition_amount = good_recognition_amount + 1
        statistic_dict[letter][recognized_letter] = statistic_dict[letter][recognized_letter] + 1

for letter in training_letters:
    for recognized_letter in training_letters:
        statistic_percentage_dict[letter][recognized_letter] = (statistic_dict[letter][recognized_letter]/float(RIGHT_INDEX - LEFT_INDEX)) * 100
