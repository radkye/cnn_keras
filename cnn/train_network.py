import matplotlib
matplotlib.use("Agg")

from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from lenet import LeNet
import matplotlib.pyplot as plt
import numpy as np
import random
import cv2
import time
import os

EPOCHS = 15
INIT_LR = 1e-3
BS = 10

print("[INFO] loading images...")
data = []
labels = []

# grab the image paths and randomly shuffle them
# imagePaths = sorted(list(paths.list_images(args["dataset"])))
# random.seed(42)
# random.shuffle(imagePaths)


training_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'W', 'U', 'V', 'X', 'Y']
letters_to_indices_map = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7,
    'I': 8, 'K': 9, 'L': 10, 'M': 11, 'N': 12, 'O': 13, 'P': 14, 'Q': 15, 'R': 16,
    'S': 17, 'T': 18, 'W': 19, 'U': 20, 'V': 21, 'X': 22, 'Y': 23}

img_rows = img_cols = 60

# letter_file_path = '/home/radkye/Documents/studia/ASLRecognizer/images/A/A_0.jpg'
# read_image = cv2.imread(letter_file_path, 0)
# gx = cv2.Sobel(read_image, cv2.CV_32F, 1, 0)
# gy = cv2.Sobel(read_image, cv2.CV_32F, 0, 1)
# sobel_sum = cv2.addWeighted(gx, 0.5, gy, 0.5, 0)
# ret, thresholded_sobel = cv2.threshold(sobel_sum, 70, 255, cv2.THRESH_BINARY)
# resized = cv2.resize(thresholded_sobel, (img_cols, img_cols))
# cv2.imwrite('/home/radkye/Documents/masterThesis/images/cnn_sobel_200_100_samples_180_img.jpg', resized)

def load_training_and_test_data(training_letter_max_id, training_image_size):
    for letter in training_letters:
        for i in range(0, training_letter_max_id):
            letter_file_path = '/home/radkye/Documents/studia/ASLRecognizer/images/' + letter + '/' + letter + '_' + str(i) + '.jpg'
            read_image = cv2.imread(letter_file_path, 0)
            blur = cv2.GaussianBlur(read_image, (7, 7), 0)
            gx = cv2.Sobel(blur, cv2.CV_32F, 1, 0)
            gy = cv2.Sobel(blur, cv2.CV_32F, 0, 1)
            sobel_sum = cv2.addWeighted(gx, 0.5, gy, 0.5, 0)
            ret, thresholded_sobel = cv2.threshold(sobel_sum, 70, 255, cv2.THRESH_BINARY)
            resized = cv2.resize(thresholded_sobel, (training_image_size, training_image_size))
            image = img_to_array(resized)
            data.append(image)
            label = letters_to_indices_map[letter]
            labels.append(label)
    
    print(len(data))

load_training_and_test_data(180, img_rows)

# scale the raw pixel intensities to the range [0, 1]
data = np.array(data, dtype="float") / 255.0
labels = np.array(labels)

# partition the data into training and testing splits using 75% of
# the data for training and the remaining 25% for testing
(trainX, testX, trainY, testY) = train_test_split(data,
    labels, test_size=0.10, random_state=42)

def shuffle_in_unison(a, b):
    assert len(a) == len(b)
    shuffled_a = np.empty(a.shape, dtype=a.dtype)
    shuffled_b = np.empty(b.shape, dtype=b.dtype)
    permutation = np.random.permutation(len(b))
    for old_index, new_index in enumerate(permutation):
        shuffled_a[new_index] = a[old_index]
        shuffled_b[new_index] = b[old_index]
    return shuffled_a, shuffled_b

trainX, trainY = shuffle_in_unison(trainX, trainY)

# for whatever in trainY:
#     print(whatever)

train_classes = np.unique(trainY)
train_nClasses = len(train_classes)
test_classes = np.unique(testY)
test_nClasses = len(test_classes)
print('Total number of train outputs : ', train_nClasses)
print('Train output classes : ', train_classes)
print('Total number of test outputs : ', test_nClasses)
print('Test output classes : ', test_classes)


# convert the labels from integers to vectors
trainY = to_categorical(trainY, num_classes=train_nClasses)
testY = to_categorical(testY, num_classes=test_nClasses)

# construct the image generator for data augmentation
aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1,
    height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
    horizontal_flip=True, fill_mode="nearest")

# initialize the model
print("[INFO] compiling model...")
model = LeNet.build(width=img_rows, height=img_cols, depth=1, classes=train_nClasses)

for layer in model.layers:
    print(layer.get_output_at(0).get_shape().as_list())

opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
model.compile(loss="binary_crossentropy", optimizer=opt,
    metrics=["accuracy"])

# train the network
print("[INFO] training network...")
# H = model.fit_generator(aug.flow(trainX, trainY, batch_size=BS),
# 	validation_data=(testX, testY), steps_per_epoch=len(trainX) // BS,
# 	epochs=EPOCHS, verbose=1)

H = model.fit(trainX, trainY, epochs=EPOCHS, batch_size=BS, validation_data=(testX, testY),
    verbose=1)

# save the model to disk
print("[INFO] serializing network...")
model.save('/home/radkye/Documents/studia/cnn_keras/cnn/cnn_sobel_200_100_d1500_d600_d200_samples_180_kernels_10.h5')  # creates a HDF5 file 'my_model.h5'

# plot the training loss and accuracy
plt.style.use("ggplot")
plt.figure()
N = EPOCHS
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend(loc="upper right")
plt.savefig('/home/radkye/Documents/studia/cnn_keras/cnn_sobel_200_100_d1500_d600_d200_samples_180_kernels_10_plot.png')
