import cv2
import numpy as np
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import operator

image_size = 60
camera_id = 0
video_capture = cv2.VideoCapture(camera_id)

training_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'W', 'U', 'V', 'X', 'Y']
letters_to_indices_map = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7,
    'I': 8, 'K': 9, 'L': 10, 'M': 11, 'N': 12, 'O': 13, 'P': 14, 'Q': 15, 'R': 16,
    'S': 17, 'T': 18, 'W': 19, 'U': 20, 'V': 21, 'X': 22, 'Y': 23}

def get_biggest_contour(contours, minimal_contour_size = 4000):
    size = 0
    result = []
    temp_id = -1
    contour_id = 0
    for contour in contours:
        temp_id = temp_id + 1
        temp_size = cv2.contourArea(contour)
        if temp_size > size:
            size = temp_size
            result = contour
            contour_id = temp_id

    return result, contour_id

def get_recognized_letter(letter_index):
    for letter in training_letters:
        if letters_to_indices_map[letter] == letter_index:
            return letter
    return " "

def get_result_index(results):
    index = -1
    temp_result = -1
    for result in results:
        if result > temp_result:
            temp_result = result
            index = index + 1
    return index

model = load_model('/home/radkye/Documents/studia/cnn_keras/cnn/cnn_sobel_200_100_d500_d200_samples_180_kernels_6_cropped_hand.h5')

while(video_capture.isOpened()):
    _, input_img = video_capture.read()
    
    hand_field_img = input_img[0:400, 0:400]
    blur = cv2.GaussianBlur(hand_field_img, (7, 7), 0)
    ycrcb_img = cv2.cvtColor(blur, cv2.COLOR_BGR2YCR_CB)
    
    ycrcb_skin_min = np.array([0, 133, 77])
    ycrcb_skin_max = np.array([255, 173, 127])
    
    mask = cv2.inRange(ycrcb_img, ycrcb_skin_min, ycrcb_skin_max)
    hand_image = cv2.bitwise_and(hand_field_img, hand_field_img, mask = mask)
    
    contour_image_whatever, contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, 2)
    
    if contours != None:
        biggest_contour, biggest_contour_id = get_biggest_contour(contours, 4000)
        cv2.drawContours(input_img, contours, biggest_contour_id, (0, 0, 255), 1)
        if biggest_contour != []:
            hand_contour_bounding_rect = cv2.boundingRect(biggest_contour)
            x, y, w, h = hand_contour_bounding_rect
            if w > 0 and h > 0:
                cv2.rectangle(input_img, (x, y), (x + w, y + h), (0, 255, 0), 1)
                rectangle_hand_image = hand_image[y : y+h, x : x+w]

    grayscale_hand_img = cv2.cvtColor(rectangle_hand_image, cv2.COLOR_BGR2GRAY)
    gx = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 1, 0)
    gy = cv2.Sobel(grayscale_hand_img, cv2.CV_32F, 0, 1)
    sobel_sum = cv2.addWeighted(gx, 0.5, gy, 0.5, 0)
    ret, thresholded_sobel = cv2.threshold(sobel_sum, 70, 255, cv2.THRESH_BINARY)
    resized = cv2.resize(thresholded_sobel, (image_size, image_size))
    normalized_to_one = resized.astype("float") / 255.0
    img_array = img_to_array(normalized_to_one)
    image_for_recognition = np.expand_dims(img_array, axis=0)

    recognition_result = model.predict(image_for_recognition)[0]
    index, max_value = max(enumerate(recognition_result), key=operator.itemgetter(1))
    if max_value > 0.9:
        recognized_letter = get_recognized_letter(index)
    else:
        recognized_letter = " "

    cv2.putText(input_img, recognized_letter, (400, 400), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 1)
    cv2.imshow('Camera capture', input_img)
    
    print('Recognized letter is: ', recognized_letter)
    
    k = 0xFF & cv2.waitKey(10)
    if k == 120:
        print("Exiting the application.")
        break
    if k == 115:
        print("Saving hand image")
        cv2.imwrite('/home/radkye/Documents/masterThesis/images/handFieldOriginal.jpg', hand_field_img)
        cv2.imwrite('/home/radkye/Documents/masterThesis/images/blurHand.jpg', blur)
        cv2.imwrite('/home/radkye/Documents/masterThesis/images/mask.jpg', mask)
        break

if (not video_capture.isOpened()):
    print("Camera was not detected.")

video_capture.release()        
cv2.destroyAllWindows()